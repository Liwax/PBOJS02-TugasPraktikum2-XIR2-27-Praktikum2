/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum2;
import java.util.Scanner;
/**
 *
 * @author Liwax
 */
public class UjiBola {
    public static void main(String[] args) {
        double jarijari;
        
        Scanner sc = new Scanner(System.in);
        
  
        //instance of class
        Bola bl = new Bola();
        
        
        //Interface awal
        System.out.println("Menghhitung Luas Permukaan dan Volume Bola");
        bl.showDiameter();
        bl.showLuasPermukaan();
        bl.showVolume();
        
        //input
        System.out.println("Masukkan jari-jari Bola : ");
        jarijari = sc.nextDouble();
        
        bl.setJarijari(jarijari);
              
        //output
        System.out.println("Luas Permukaan dan Volume Bola");
        bl.showDiameter();
        bl.showLuasPermukaan();
        bl.showVolume();
    }
}
