/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum2;

/**
 *
 * @author Liwax
 */
public class Bola 
{
    //deklarasi variabel
    private double jarijari;
    
    //method setter
    public void setJarijari(double newJarijari){
        jarijari = newJarijari;
    }
    
    //method tambahan
    public void showDiameter(){
        double diameter = 2 * jarijari;
        System.out.println("Diameter : " + diameter);
    }
    
    public void showLuasPermukaan(){
        double luasPermukaan = 4 * Math.PI * Math.pow(jarijari, 2) ;
        System.out.println("Luas Permukaan : " + luasPermukaan);
    }
    
     public void showVolume(){
        double volume = 4 / 3 * Math.PI * Math.pow(jarijari, 3) ;
        System.out.println("Volume : " + volume);
    }
}
